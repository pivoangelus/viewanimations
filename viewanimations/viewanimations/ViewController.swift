//
//  ViewController.swift
//  viewanimations
//
//  Created by Justin Andros on 3/29/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

func delay(seconds seconds: Double, completion:() -> ()) {
    let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(Double(NSEC_PER_SEC) * seconds))
    dispatch_after(popTime, dispatch_get_main_queue()) {
        completion()
    }
}

func tintBackgroundColor(layer layer: CALayer, toColor: UIColor) {
    let tint = CABasicAnimation(keyPath: "tint")
    tint.fromValue = layer.backgroundColor
    tint.toValue = toColor.CGColor
    tint.duration = 1.0
    layer.addAnimation(tint, forKey: nil)
    layer.backgroundColor = toColor.CGColor
}

func roundCorners(layer layer: CALayer, toRadius: CGFloat) {
    let corners = CABasicAnimation(keyPath: "corners")
    corners.fromValue = layer.cornerRadius
    corners.toValue = toRadius
    corners.duration = 0.33
    layer.addAnimation(corners, forKey: nil)
    layer.cornerRadius = toRadius
}

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var loginButton: UIButton!
    @IBOutlet var heading: UILabel!
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var cloud1: UIImageView!
    @IBOutlet var cloud2: UIImageView!
    @IBOutlet var cloud3: UIImageView!
    @IBOutlet var cloud4: UIImageView!
    
    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    let status = UIImageView(image: UIImage(named: "banner"))
    var statusPosition = CGPoint.zero
    let label = UILabel()
    let messages = ["Connecting ...", "Authorizing ...", "Sending credentials ...", "Failed"]
    let info = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 8.0
        loginButton.layer.masksToBounds = true
        
        spinner.frame = CGRect(x: -20.0, y: 6.0, width: 20.0, height: 20.0)
        spinner.startAnimating()
        spinner.alpha = 0.0
        loginButton.addSubview(spinner)
        
        status.hidden = true
        status.center = loginButton.center
        view.addSubview(status)
        
        label.frame = CGRect(x: 0.0, y: 0.0, width: status.frame.size.width, height: status.frame.size.height)
        label.font = UIFont(name: "HelveticaNeue", size: 18.0)
        label.textColor = UIColor(red: 0.89, green: 0.38, blue: 0.0, alpha: 1.0)
        label.textAlignment = .Center
        status.addSubview(label)
        statusPosition = status.center
        
        info.frame = CGRect(x: 0.0, y: loginButton.center.y + 60.0, width: view.frame.size.width, height: 30.0)
        info.backgroundColor = UIColor.clearColor()
        info.font = UIFont(name: "HelveticaNeue", size: 12.0)
        info.textAlignment = .Center
        info.textColor = UIColor.whiteColor()
        info.text = "Tap on a field and enter username and password"
        view.insertSubview(info, belowSubview: loginButton)
        
        let flyLeft = CABasicAnimation(keyPath: "position.x")
        flyLeft.fromValue = info.layer.position.x + view.frame.size.width
        flyLeft.toValue = info.layer.position.x
        flyLeft.duration = 5.0
        info.layer.addAnimation(flyLeft, forKey: "infoappear")
        
        let fadeLabelIn = CABasicAnimation(keyPath: "opacity")
        fadeLabelIn.fromValue = 0.2
        fadeLabelIn.toValue = 1.0
        fadeLabelIn.duration = 4.5
        info.layer.addAnimation(fadeLabelIn, forKey: "fadein")
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let moveClouds = CABasicAnimation(keyPath: "opacity")
        moveClouds.fromValue = 0.0
        moveClouds.toValue = 1.0
        moveClouds.duration = 0.5
        moveClouds.fillMode = kCAFillModeBackwards
        moveClouds.beginTime = CACurrentMediaTime() + 0.5
        cloud1.layer.addAnimation(moveClouds, forKey: nil)
        moveClouds.beginTime = CACurrentMediaTime() + 0.7
        cloud2.layer.addAnimation(moveClouds, forKey: nil)
        moveClouds.beginTime = CACurrentMediaTime() + 0.9
        cloud3.layer.addAnimation(moveClouds, forKey: nil)
        moveClouds.beginTime = CACurrentMediaTime() + 1.1
        cloud4.layer.addAnimation(moveClouds, forKey: nil)
        
        loginButton.center.y += 30.0
        loginButton.alpha = 0.0
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let flyRight = CABasicAnimation(keyPath: "position.x")
        flyRight.fromValue = -view.bounds.size.width / 2
        flyRight.toValue = view.bounds.size.width / 2
        flyRight.duration = 0.5
        flyRight.fillMode = kCAFillModeBoth
        flyRight.delegate = self
        flyRight.setValue("form", forKey: "name")
        
        flyRight.setValue(heading.layer, forKey: "layer")
        heading.layer.addAnimation(flyRight, forKey: nil)

        flyRight.setValue(username.layer, forKey: "layer")
        flyRight.beginTime = CACurrentMediaTime() + 0.3
        username.layer.addAnimation(flyRight, forKey: nil)
        username.layer.position.x = view.bounds.size.width / 2
        
        flyRight.setValue(password.layer, forKey: "layer")
        flyRight.beginTime = CACurrentMediaTime() + 0.4
        password.layer.addAnimation(flyRight, forKey: nil)
        password.layer.position.x = view.bounds.size.width / 2
        
//        UIView.animateWithDuration(0.5, delay: 0.5, options: [], animations: {
//            self.cloud1.alpha = 1.0
//            }, completion: { _ in
//                self.animateCloudv1(self.cloud1)
//        })
//        UIView.animateWithDuration(0.5, delay: 0.7, options: [], animations: {
//            self.cloud2.alpha = 1.0
//            }, completion: { _ in
//                self.animateCloudv1(self.cloud2)
//        })
//        UIView.animateWithDuration(0.5, delay: 0.9, options: [], animations: {
//            self.cloud3.alpha = 1.0
//            }, completion: { _ in
//                self.animateCloudv1(self.cloud3)
//        })
//        UIView.animateWithDuration(0.5, delay: 1.1, options: [], animations: {
//            self.cloud4.alpha = 1.0
//            }, completion: { _ in
//                self.animateCloudv1(self.cloud4)
//        })
        
        animateCloudv2(cloud1.layer)
        animateCloudv2(cloud2.layer)
        animateCloudv2(cloud3.layer)
        animateCloudv2(cloud4.layer)
        
        UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: {
            self.loginButton.center.y -= 30.0
            self.loginButton.alpha = 1.0
            }, completion: nil)
        
        username.delegate = self
        password.delegate = self
    }
    
    @IBAction func login() {
        view.endEditing(true)
        
        UIView.animateWithDuration(1.5, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: [], animations: {
            self.loginButton.bounds.size.width += 80.0
            }, completion: { _ in
                self.showMessage(index: 0)
        })
        UIView.animateWithDuration(0.33, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: [], animations: {
            self.loginButton.center.y += 60.0
            self.spinner.center = CGPoint(x: 40.0, y: self.loginButton.frame.size.height / 2)
            self.spinner.alpha = 1.0
            self.loginButton.enabled = false
            }, completion: nil)
        
        let tintColor = UIColor(red: 0.85, green: 0.83, blue: 0.45, alpha: 1.0)
        tintBackgroundColor(layer: loginButton.layer, toColor: tintColor)
        roundCorners(layer: loginButton.layer, toRadius: 25.0)
    }
    
    func showMessage(index index: Int) {
        label.text = messages[index]
        
        UIView.transitionWithView(status, duration: 0.33, options: [.CurveEaseOut, .TransitionCurlDown], animations: {
            self.status.hidden = false
            }, completion: { _ in
                delay(seconds: 2.0) {
                    if index < self.messages.count - 1 {
                        self.removeMessage(index: index)
                    } else {
                        self.resetForm()
                    }
                }
        })
    }
    
    func removeMessage(index index: Int) {
        UIView.animateWithDuration(0.33, delay: 0.0, options: [], animations: {
            self.status.center.x += self.view.frame.size.width
            }, completion: { _ in
                self.status.hidden = true
                self.status.center = self.statusPosition
                self.showMessage(index: index + 1)
        })
    }
    
    func resetForm() {
        UIView.transitionWithView(status, duration: 0.2, options: [.CurveEaseOut,.TransitionCurlUp], animations: {
            self.status.hidden = true
            }, completion: { _ in
                UIView.animateWithDuration(0.5, delay: 0.0, options: [], animations: {
                    self.spinner.center = CGPoint(x: -20.0, y: 16.0)
                    self.spinner.alpha = 0.0
                    self.loginButton.bounds.size.width -= 80.0
                    self.loginButton.center.y -= 60.0
                    }, completion: { _ in
                        self.loginButton.enabled = true
                        let tintColor = UIColor(red: 0.63, green: 0.84, blue: 0.35, alpha: 1.0)
                        tintBackgroundColor(layer: self.loginButton.layer, toColor: tintColor)
                        roundCorners(layer: self.loginButton.layer, toRadius: 10.0)
                })
        })
    }
    
    func animateCloudv1(cloud: UIImageView) {
        let cloudSpeed = 10.0 / view.frame.size.width
        let duration = (view.frame.size.width - cloud.frame.origin.x) * cloudSpeed
        
        UIView.animateWithDuration(NSTimeInterval(duration), delay: 0.0, options: [.CurveLinear], animations: {
            cloud.frame.origin.x = self.view.frame.size.width
            }, completion: { _ in
                cloud.frame.origin.x = -cloud.frame.size.width
                self.animateCloudv1(cloud)
        })
    }
    
    func animateCloudv2(layer: CALayer) {
        let cloudSpeed = 6.0 / Double(view.layer.frame.size.width)
        let duration: NSTimeInterval = Double(view.layer.frame.size.width - layer.frame.origin.x) * cloudSpeed
        
        let cloudMove = CABasicAnimation(keyPath: "position.x")
        cloudMove.duration = duration
        cloudMove.toValue = self.view.bounds.size.width + layer.bounds.width / 2
        cloudMove.delegate = self
        cloudMove.setValue("cloud", forKey: "name")
        cloudMove.setValue(layer, forKey: "layer")
        
        layer.addAnimation(cloudMove, forKey: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextField = (textField === username) ? password : username
        nextField.becomeFirstResponder()
        return true
    }
    
    // MARK: CAAnimation delegates
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let name = anim.valueForKey("name") as? String {
            if name == "form" {
                print("field slide in")
                let layer = anim.valueForKey("layer") as? CALayer
                anim.setValue(nil, forKey: "layer")
                
                let pulse = CABasicAnimation(keyPath: "transform.scale")
                pulse.fromValue = 1.25
                pulse.toValue = 1.0
                pulse.duration = 0.25
                layer?.addAnimation(pulse, forKey: nil)
            }
            
            if name == "cloud" {
                print("cloud reset")
                if let layer = anim.valueForKey("layer") as? CALayer {
                    anim.setValue(nil, forKey: "layer")
                    
                    layer.position.x = -layer.bounds.width / 2
                    delay(seconds: 0.5, completion: {
                        self.animateCloudv2(layer)
                    })
                }
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print(info.layer.animationKeys())
        info.layer.removeAnimationForKey("infoappear")
    }

}